<?php
/*
 * This file has been generated automatically.
 * Please change the configuration for correct use deploy.
 */

require 'recipe/symfony.php';

// Set configurations
set('repository', 'https://kevinkroos@bitbucket.org/kevinkroos/lightapp.git');
set('shared_files', ['app/config/parameters.yml']);
set('shared_dirs', ['var/logs', 'var/sessions']);
set('writable_dirs', ['var/cache', 'var/logs', 'var/sessions']);
set('bin_dir', 'bin');
set('var_dir', 'var');

// Configure servers
server('production', 'developmentlocatie.nl')
    ->user('admin')
    ->password()
    ->env('deploy_path', '/home/admin/web/developmentlocatie.nl/public_html');

server('beta', 'beta.domain.com')
    ->user('username')
    ->password()
    ->env('deploy_path', '/var/www/beta.domain.com');

/**
 * Restart php-fpm on success deploy.
 */
task('php-fpm:restart', function () {
    // Attention: The user must have rights for restart service
    // Attention: the command "sudo /bin/systemctl restart php-fpm.service" used only on CentOS system
    // /etc/sudoers: username ALL=NOPASSWD:/bin/systemctl restart php-fpm.service
    run('sudo /bin/systemctl restart php-fpm.service');
})->desc('Restart PHP-FPM service');

//after('success', 'php-fpm:restart');


/**
 * Attention: This command is only for for example. Please follow your own migrate strategy.
 * Attention: Commented by default.  
 * Migrate database before symlink new release.
 */
 
// before('deploy:symlink', 'database:migrate');