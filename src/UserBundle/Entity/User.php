<?php

namespace UserBundle\Entity;

use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="fos_user")
 */
class User extends BaseUser
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Token", mappedBy="user")
     */
    public $token;

    public function __construct()
    {
        parent::__construct();
        // your own logic
    }

    /**
     * Add token
     *
     * @param \AppBundle\Entity\Token $token
     *
     * @return User
     */
    public function addToken(\AppBundle\Entity\Token $token)
    {
        $this->token[] = $token;

        return $this;
    }

    /**
     * Remove token
     *
     * @param \AppBundle\Entity\Token $token
     */
    public function removeToken(\AppBundle\Entity\Token $token)
    {
        $this->token->removeElement($token);
    }

    /**
     * Get token
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getToken()
    {
        return $this->token;
    }
}
