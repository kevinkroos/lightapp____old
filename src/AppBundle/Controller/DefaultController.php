<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="homepage")
     */
    public function indexAction(Request $request)
    {
        return $this->render('default/index.html.twig');
    }

    /**
     * @Route("/test", name="test")
     */
    public function testAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        //$photos = $em->getRepository('AppBundle:Photo')->findAll();

        $user = $em
            ->getRepository('UserBundle:User')
            ->find(2);

        $tkn = $user->getToken()->toArray();

        return $this->render('default/test.html.twig', ['user' => $tkn]);
    }
}
