<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Token;
use AppBundle\Form\TokenType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;

class TokenControllerController extends Controller
{
    /**
     * @Route("/token", name="token")
     */
    public function IndexAction(Request $request)
    {
        $token = new Token();

        $form = $this->createForm(TokenType::class, $token);

        $form->handleRequest($request);

//        print_r($form);

        if ($form->isSubmitted()) {

            var_dump($form->getData());die();
        }

        return $this->render('token/index.html.twig', ['form' => $form->createView()]);
    }
}
