var gulp = require('gulp');
var concat = require('gulp-concat');
var concatCss = require('gulp-concat-css');
var cleanCSS = require('gulp-clean-css');

gulp.task('concat', function () {
    return gulp.src([
            'bower_components/bootstrap/dist/css/bootstrap.min.css',
            'bower_components/font-awesome/css/font-awesome.min.css',
            'bower_components/morrisjs/morris.css',
            'web/assets/custom/stylesheet.css'
        ])
        .pipe(concatCss("app.css"))
        .pipe(cleanCSS({compatibility: 'ie8'}))
        .pipe(gulp.dest('web/assets/css/'));
});

gulp.task("fonts", function () {
    return gulp.src("bower_components/font-awesome/fonts/*")
        .pipe(gulp.dest("web/font-awesome/fonts/"));
});

gulp.task('scripts', function () {
    return gulp.src([
            'bower_components/jquery/dist/jquery.js',
            'bower_components/bootstrap/dist/js/bootstrap.js',
            'bower_components/raphael/raphael.js',
            'bower_components/morrisjs/morris.js',
            'web/assets/custom/morris-data.js'
        ])
        .pipe(concat('app.js'))
        .pipe(gulp.dest('web/assets/scripts/'));
});

gulp.task("default", function () {
    console.log("Werkt");
});